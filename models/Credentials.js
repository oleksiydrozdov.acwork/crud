const {Schema, model} = require('mongoose');

const credentialsSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = model('Credentials', credentialsSchema);
