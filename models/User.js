const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,

  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('User', userSchema);
