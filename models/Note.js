const {Schema, model} = require('mongoose');

const userNote = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },

});

module.exports = model('Note', userNote);
