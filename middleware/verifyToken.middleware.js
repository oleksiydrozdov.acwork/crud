const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];

    if (!token) {
      return res.status(400).json({message: 'User is not authorized'});
    }

    const {userId, username} = jwt.verify(token, config.get('jwt_secret'));

    req.params.username = username;
    req.params.userId = userId;
    next();
  } catch (e) {
    return res.status(400).json({message: 'User is not authorized'});
  }
};
