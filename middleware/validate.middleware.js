const Joi = require('joi');

module.exports.authValidate = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),

  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};


module.exports.userPatchValidate = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};
