const Credentials = require('../models/Credentials');
const User = require('../models/User');

const getProfile = async (req, res, next) => {
  try {
    const {username} = req.params;
    const user = await User.findOne({username});
    if (!user) {
      return res.status(400).json({message: 'User has no found'});
    }
    res.json({user: user});
    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const deleteProfile = async (req, res, next) => {
  try {
    const {username} = req.params;
    const user = await User.findOneAndRemove({username});
    const credentials = await Credentials.findOneAndRemove({username});
    if (!user || !credentials) {
      return res.status(400).json({message: 'User has no found'});
    }
    res.json({message: 'Succses'});
    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};


module.exports={
  getProfile,
  deleteProfile
}