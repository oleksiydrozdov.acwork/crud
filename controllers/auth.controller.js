const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const Credentials = require('../models/Credentials');
const User = require('../models/User');

const register = async (req, res, next) => {
  try {
    const {username, password} = req.body;

    const candidate = await Credentials.findOne({username});

    if (candidate) {
      return res.status(400).json({message: 'User has already exists'});
    }

    const hashedPassword = await bcrypt.hash(password, 12);
    const credentials = new Credentials({username, password: hashedPassword});
    await credentials.save();

    const user = new User({username});
    await user.save();

    res.json({message: 'User has been registered succesfully'});

    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const login = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    const user = await Credentials.findOne({username});

    if (!user) {
      return res.status(400).json({message: 'User has no found'});
    }

    const isMatched = await bcrypt.compare(password, user.password);

    if (!isMatched) {
      return res.status(400).json({message: 'Wrong password'});
    }

    const token = jwt.sign(
        {username: user.username, userId: user._id},
        config.get('jwt_secret'),
        {expiresIn: '1h'},
    );

    res.json({message: 'Succses', jwt_token: token});

    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

module.exports ={
  register,
  login
}