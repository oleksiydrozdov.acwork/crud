const Note = require('../models/Note');

const getNotes = async (req, res, next) => {
  try {
    const {userId} = req.params;

    const notes = await Note.find({userId});

    if (!notes) {
      return res.status(400).json({message: 'Notes has no found'});
    }

    res.json({notes: notes});

    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const addNote = async (req, res, next) => {
  try {
    const {text} = req.body;
    const {userId} = req.params;

    const note = new Note({text, userId});

    await note.save();

    res.json({message: 'Succses'});

    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const getNoteById = async (req, res, next) => {
  try {
    const {id, userId} = req.params;

    const note = await Note.find({_id: id, userId});

    if (!note) {
      return res.status(400).json({message: 'Note has no found'});
    }
    res.json({note: note});
    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const updateNoteById = async (req, res, next) =>{
  try {
    const {text} = req.body;
    const {id, userId} = req.params;

    const note = await Note.findOneAndUpdate({userId, _id: id}, {text});

    if (!note) {
      return res.status(400).json({message: 'Note has no found'});
    }

    res.json({message: 'Succses'});
    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const patchNoteById = async (req, res, next) =>{
  try {
    const {id, userId} = req.params;
    const {completed} = await Note.find({userId, _id: id});

    const update = await Note.findOneAndUpdate(
        {userId, _id: id},
        {completed: !completed},
    );

    if (!update) {
      return res.status(400).json({message: 'Note has no found'});
    }

    res.json({message: 'Succses'});
    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const deleteNoteById = async (req, res, next) =>{
  try {
    const {id, userId} = req.params;

    const note = await Note.findOneAndDelete({_id: id, userId});

    if (!note) {
      return res.status(400).json({message: 'Note has no found'});
    }

    res.json({message: 'Succses'});
    next();
  } catch (e) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

module.exports={
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  patchNoteById,
  deleteNoteById
}