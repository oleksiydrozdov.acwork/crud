const express = require('express');
const verifyToken = require('../middleware/verifyToken.middleware');
const {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  patchNoteById,
  deleteNoteById} = require('../controllers/note.controller');
const router = new express.Router();

router.get('/', verifyToken, getNotes);
router.post('/', verifyToken, addNote);
router.get('/:id', verifyToken, getNoteById);
router.put('/:id', verifyToken, updateNoteById);
router.patch('/:id', verifyToken, patchNoteById);
router.delete('/:id', verifyToken, deleteNoteById);

module.exports = router;
