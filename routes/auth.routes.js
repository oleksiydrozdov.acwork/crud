const express = require('express');
const {authValidate} = require('../middleware/validate.middleware');
const {register, login} = require('../controllers/auth.controller');
const router = new express.Router();

router.post('/register', authValidate, register);
router.post('/login', authValidate, login);

module.exports = router;
