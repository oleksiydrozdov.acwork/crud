const express = require('express');
const {userPatchValidate} = require('../middleware/validate.middleware');
const verifyToken = require('../middleware/verifyToken.middleware');
const userControllers = require('../controllers/user.controllers');
const {getProfile, deleteProfile} = userControllers;
const router = new express.Router();

router.get('/me', verifyToken, getProfile);
router.delete('/me', verifyToken, deleteProfile);
router.patch('/me', userPatchValidate, verifyToken);

module.exports = router;
